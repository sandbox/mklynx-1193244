<?php
// $Id$

/**
 * @file
 * file that holds all the specific functions of the products import
 */

/**
 * Load a single record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function cpi_products_load($id, $reset = FALSE) {
  return cpi_products_load_multiple(array($id), $reset);
}

/**
 * This function returns a array of product records and has as input a array of AID numbers.
 */
function cpi_products_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('cpi_products', $ids, $conditions, $reset);
}

/**
 * This function saves a product array of information to the database table.
 * If the record already exists it will be over written. This is where the fields AID and CREATED are filled.
 */
function cpi_products_save($product) {

  $result = db_query("SELECT aid FROM {cpi_products} WHERE productid=:proid", array(':proid' => $product->productid,
    ));
  $sid              = $result->fetchField(0);
  $product->created = REQUEST_TIME;
  $product->aid     = $sid;
  $test             = cpi_products_load($sid);
  if ($test[$sid]->productid = $product->productid) {
    entity_delete('cpi_products', $sid);
  }
  entity_save('cpi_products', $product);
  return $product->aid;
}

/**
 * This function returns a product array. Below is the mapping table for the input of this function.
 */
function cpi_container_products($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o) {
  $container = array(
    'aid' => '',
    'productid' => $a,
    'name' => $b,
    'merchandiseId' => $c,
    'sellPrice' => $d,
    'marketplacePrice' => $e,
    'currencyName' => $f,
    'description' => $g,
    'storeId' => $h,
    'sectionId' => $i,
    'defaultCaption' => $j,
    'categoryId' => $k,
    'categoryCaption' => $l,
    'producturl' => $m,
    'imageurl280' => $n,
    'imageurl480' => $o,
    'created' => '',
  );
  $test = entity_create('cpi_products', $container);

  return $test;
}

/**
 * This function gets all the products in a given storesection from cafepress and
 * saves them into a file called “public://cafepress/products$section_number.xml”
 */
function cpi_listproductsbyss($section_number, $store) {



  $user = variable_get('cafepress_import_userid', '');
  $api  = variable_get('cafepress_import_api_key', '');
  $pass = variable_get('cafepress_import_password', '');


  $caferequest  = "http://open-api.cafepress.com/product.listByStoreSection.cp?v=3&appKey=$api&storeId=$store&sectionId=$section_number";
  $result       = cpstore_get_web_page($caferequest);
  $file_content = $result['content'];

  // Safe Output of authentication for Cafepress.

  $result = file_save_data($file_content, "public://cafepress/products" . $section_number . ".xml", $replace = FILE_EXISTS_REPLACE);
}

/*
 * This function is the main part of the products this puts all the other functions to use and
 * gets all the products from 1 store a saves them to the products database.
 */

function cpi_get_all_products($store_name) {
  authentication_cafepress();
  $result = db_query("SELECT sectionid FROM {cpi_storesection}");
  $section = $result->fetchall();

  foreach ($section as $z) {
    foreach ($z as $y) {
      cpi_listproductsbyss($y, $store_name);

      // XML feed file/URL
      $xmlurl0 = "public://cafepress/products" . $y . ".xml";
      $xmlstr0 = file_get_contents($xmlurl0);
      $xmlobj0 = simplexml_load_string($xmlstr0);
      $arrxml0 = objectsIntoArray($xmlobj0);

      foreach ($arrxml0 as $a) {

        foreach ($a as $b) {


          /*
           echo $b['@attributes']['id'],"<br />";
           echo $b['@attributes']['name'],"<br />";
           echo $b['@attributes']['merchandiseId'],"<br />";
           echo $b['@attributes']['sellPrice'],"<br />";
           echo $b['@attributes']['marketplacePrice'],"<br />";
           echo $b['@attributes']['currencyName'],"<br />";
           echo $b['@attributes']['description'],"<br />";
           echo $b['@attributes']['storeId'],"<br />";
           echo $b['@attributes']['sectionId'],"<br />";
           echo $b['@attributes']['defaultCaption'],"<br />";
           echo $b['@attributes']['categoryId'],"<br />";
           echo $b['@attributes']['categoryCaption'],"<br />";
           echo $b['@attributes']['storeUri'],"<br />";
           echo $b['productImage']['2']['@attributes']['productUrl'],"<br />";
           echo $b['productImage']['0']['@attributes']['productUrl'],"<br />";
           print "<br />";
           print "next";
           print "<br />";
         */


          $prod_con = cpi_container_products(
            $b['@attributes']['id'],
            $b['@attributes']['name'],
            $b['@attributes']['merchandiseId'],
            $b['@attributes']['sellPrice'],
            $b['@attributes']['marketplacePrice'],
            $b['@attributes']['currencyName'],
            $b['@attributes']['description'],
            $b['@attributes']['storeId'],
            $b['@attributes']['sectionId'],
            $b['@attributes']['defaultCaption'],
            $b['@attributes']['categoryId'],
            $b['@attributes']['categoryCaption'],
            $b['@attributes']['storeUri'],
            $b['productImage']['2']['@attributes']['productUrl'],
            $b['productImage']['0']['@attributes']['productUrl']
          );

          $check = cpi_products_save($prod_con);
        }
        // end foreach $a as $b
      }
      // end foreach ($arrxml0 as $a )
    }
    // end foreach ($z as $y )
  }
  // end foreach ($section as $z )
}

