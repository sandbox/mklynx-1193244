<?php
// $Id$

/**
 * @file
 * file that holds all the specific functions of the storesetion import
 */

/**
 * Load a single record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function cpi_storesection_load($id, $reset = FALSE) {
  return cpi_storesection_load_multiple(array($id), $reset);
}

/**
 * Load multiple records.
 * This function returns a array of storesection records and has as input a array of AID numbers
 */
function cpi_storesection_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('cpi_storesection', $ids, $conditions, $reset);
}

/**
 * This function saves a storesection array of information to the database table.
 * If the record already exists it will be over written.
 */
function cpi_storesection_save($storesection) {

  $result = db_query("SELECT aid FROM {cpi_storesection} WHERE sectionid=:proid", array(':proid' => $storesection->sectionid,
    ));
  $sid = $result->fetchField(0);
  $storesection->created = REQUEST_TIME;
  $storesection->aid = $sid;
  $test = cpi_storesection_load($sid);
  if ($test[$sid]->sectionid = $storesection->sectionid) {
    entity_delete('cpi_storesection', $sid);
  }
  entity_save('cpi_storesection', $storesection);
  return $storesection->aid;
}

/** create container for storesection */
function cpi_container_storesection($a, $b, $c, $d, $e) {
  $container = array(
    'aid' => '',
    'sectionid' => $a,
    'storeid' => $b,
    'parent' => $c,
    'caption' => $d,
    'description' => $e,
    'created' => '',
  );
  $test = entity_create('cpi_storesection', $container);
  return $test;
}

/**
 * This function gets all the storesections from cafepress and
 * saves them into a file called “public://cafepress/storesection$section_number.xml”
 */
function cpi_list_storesections($section_number, $store) {
  $user = variable_get('cafepress_import_userid', '');
  $api  = variable_get('cafepress_import_api_key', '');
  $pass = variable_get('cafepress_import_password', '');

  authentication_cafepress();
  $caferequest  = "http://open-api.cafepress.com/store.listStoreSubSections.cp?v=3&appKey=$api&storeId=$store&storeSectionid=$section_number";
  $result       = cpstore_get_web_page($caferequest);
  $file_content = $result['content'];

  // Safe Output of authentication for Cafepress.

  $result = file_save_data($file_content, "public://cafepress/storesection" . $section_number . ".xml", $replace = FILE_EXISTS_REPLACE);
}

/**
 * This function is the main part of the storesection this puts all the other functions to use and
 * gets all the storesections from 1 store a saves them to the storesection database.
 */
function cpi_get_all_storesections($store_name) {



  cpi_list_storesections(0, $store_name);

  // XML feed file/URL
  $xmlurl0 = "public://cafepress/storesection0.xml";
  $xmlstr0 = file_get_contents($xmlurl0);
  $xmlobj0 = simplexml_load_string($xmlstr0);
  $arrxml0 = objectsIntoArray($xmlobj0);


  foreach ($arrxml0 as $a) {
    $cnt = count($a);
    switch ($cnt) {
      case 0:
        break;

      case 1:
        foreach ($a as $b) {

          /** for testing purpuses only
           echo $b['id'],"<br />";
           echo $b['storeId'],"<br />";
           echo $b['parentSectionId'],"<br />";
           echo $b['caption'],"<br />";
           echo $b['description'],"<br />";
           echo "<br />";
           echo $b['id'];
           */
          $ss_con = cpi_container_storesection(
            $b['id'],
            $b['storeId'],
            $b['parentSectionId'],
            $b['caption'],
            $b['description']
          );

          $check = cpi_storesection_save($ss_con);
          cpi_list_storesections($b['@attributes']['id'], $store_name);

          $xmlurl1 = "public://cafepress/storesection" . $b['id'] . ".xml";
          if (file_exists($xmlurl1)) {
            $xmlstr1 = file_get_contents($xmlurl1);
          }
          else {
            $xmlstr1 = "";
          }
          $xmlobj1 = simplexml_load_string($xmlstr1);
          $arrxml1 = objectsIntoArray($xmlobj1);

          foreach ($arrxml1 as $c) {
            $cnt1 = count($c);
            switch ($cnt1) {
              case 1:
                foreach ($c as $d) {

                    /** for testing purpuses only
                     echo "second ===>",$d['id'],"<br />";
                     echo "second ===>",$d['storeId'],"<br />";
                     echo "second ===>",$d['parentSectionId'],"<br />";
                     echo "second ===>",$d['caption'],"<br />";
                     echo "second ===>",$d['description'],"<br />";
                     echo "<br />";
                     */
                    $ss_con = cpi_container_storesection(
                      $d['id'],
                      $d['storeId'],
                      $d['parentSectionId'],
                      $d['caption'],
                      $d['description']
                    );

                    $check = cpi_storesection_save($ss_con);

                    cpi_list_storesections($d['id'], $store_name);

                    $xmlurl2 = "public://cafepress/storesection" . $d['id'] . ".xml";
                    if (file_exists($xmlurl2)) {
                        $xmlstr2 = file_get_contents($xmlurl2);
                      }
                      else {
                          $xmlstr2 = "";
                        }
                        $xmlobj2 = simplexml_load_string($xmlstr2);
                        $arrxml2 = objectsIntoArray($xmlobj2);

                        foreach ($arrxml2 as $z) {
                            $cntz = count($z);
                            switch ($cntz) {
                                case 1:
                                  foreach ($z as $y) {

                                        /** for testing purpuses only
                                         echo "third ===>",$y['id'],"<br />";
                                         echo "third ===>",$y['storeId'],"<br />";
                                         echo "third ===>",$y['parentSectionId'],"<br />";
                                         echo "third ===>",$y['caption'],"<br />";
                                         echo "third ===>",$y['description'],"<br />";
                                         echo "<br />";
                                         */
                                        $ss_con = cpi_container_storesection(
                                          $y['id'],
                                          $y['storeId'],
                                          $y['parentSectionId'],
                                          $y['caption'],
                                          $y['description']
                                        );

                                        $check = cpi_storesection_save($ss_con);
                                      }
                                      // end foreach ($z as $y)
                                      break;

                                    default:
                                      foreach ($z as $y) {

                                            /** for testing purpuses only
                                             echo "third ===>",$y['@attributes']['id'],"<br />";
                                             echo "third ===>",$y['@attributes']['storeId'],"<br />";
                                             echo "third ===>",$y['@attributes']['parentSectionId'],"<br />";
                                             echo "third ===>",$y['@attributes']['caption'],"<br />";
                                             echo "third ===>",$y['@attributes']['description'],"<br />";
                                             echo "<br />";
                                             */
                                            $ss_con = cpi_container_storesection(
                                              $y['@attributes']['id'],
                                              $y['@attributes']['storeId'],
                                              $y['@attributes']['parentSectionId'],
                                              $y['@attributes']['caption'],
                                              $y['@attributes']['description']
                                            );

                                            $check = cpi_storesection_save($ss_con);
                                          }
                                          // end foreach ($z as $y)
                                      }
                                      //end switch
                                    }
                                    // end foreach ($arrxml2 as $z)
                                  }
                                  // end foreach ($c as $d)
                                  break;

                                default:
                                  foreach ($c as $d) {

                                      /** for testing purpuses only
                                       echo "second ===>",$d['@attributes']['id'],"<br />";
                                       echo "second ===>",$d['@attributes']['storeId'],"<br />";
                                       echo "second ===>",$d['@attributes']['parentSectionId'],"<br />";
                                       echo "second ===>",$d['@attributes']['caption'],"<br />";
                                       echo "second ===>",$d['@attributes']['description'],"<br />";
                                       echo "<br />";
                                       */
                                      $ss_con = cpi_container_storesection(
                                        $d['@attributes']['id'],
                                        $d['@attributes']['storeId'],
                                        $d['@attributes']['parentSectionId'],
                                        $d['@attributes']['caption'],
                                        $d['@attributes']['description']
                                      );

                                      $check = cpi_storesection_save($ss_con);
                                      cpi_list_storesections($d['id'], $store_name);

                                      $xmlurl2 = ".public://cafepress/storesection" . $d['@attributes']['id'] . ".xml";
                                      if (file_exists($xmlurl2)) {
                                          $xmlstr2 = file_get_contents($xmlurl2);
                                        }
                                        else {
                                            $xmlstr2 = "";
                                          }
                                          $xmlobj2 = simplexml_load_string($xmlstr2);
                                          $arrxml2 = objectsIntoArray($xmlobj2);

                                          foreach ($arrxml2 as $z) {
                                              $cntz = count($z);
                                              switch ($cntz) {
                                                  case 1:
                                                    foreach ($z as $y) {

                                                          /** for testing purpuses only
                                                           echo "third ===>",$y['id'],"<br />";
                                                           echo "third ===>",$y['storeId'],"<br />";
                                                           echo "third ===>",$y['parentSectionId'],"<br />";
                                                           echo "third ===>",$y['caption'],"<br />";
                                                           echo "third ===>",$y['description'],"<br />";
                                                           echo "<br />";
                                                           */
                                                          $ss_con = cpi_container_storesection(
                                                            $y['id'],
                                                            $y['storeId'],
                                                            $y['parentSectionId'],
                                                            $y['caption'],
                                                            $y['description']
                                                          );

                                                          $check = cpi_storesection_save($ss_con);
                                                        }
                                                        // end foreach ($z as $y)
                                                        break;

                                                      default:
                                                        foreach ($z as $y) {

                                                              /** for testing purpuses only
                                                               echo "third ===>",$y['@attributes']['id'],"<br />";
                                                               echo "third ===>",$y['@attributes']['storeId'],"<br />";
                                                               echo "third ===>",$y['@attributes']['parentSectionId'],"<br />";
                                                               echo "third ===>",$y['@attributes']['caption'],"<br />";
                                                               echo "third ===>",$y['@attributes']['description'],"<br />";
                                                               echo "<br />";
                                                               */
                                                              $ss_con = cpi_container_storesection(
                                                                $y['@attributes']['id'],
                                                                $y['@attributes']['storeId'],
                                                                $y['@attributes']['parentSectionId'],
                                                                $y['@attributes']['caption'],
                                                                $y['@attributes']['description']
                                                              );

                                                              $check = cpi_storesection_save($ss_con);
                                                            }
                                                            // end foreach ($z as $y)
                                                        }
                                                        //end switch
                                                      }
                                                      // end foreach ($arrxml2 as $z)
                                                    }
                                                    // end foreach ($c as $d)
                                                }
                                                //end switch
                                              }
                                              // end foreach ($arrxml1 as $c)
                                            }
                                            // end foreach ($a as $b)
                                            break;

                                          default:
                                            foreach ($a as $b) {

                                              /** for testing purpuses only
                                               echo $b['@attributes']['id'],"<br />";
                                               echo $b['@attributes']['storeId'],"<br />";
                                               echo $b['@attributes']['parentSectionId'],"<br />";
                                               echo $b['@attributes']['caption'],"<br />";
                                               echo $b['@attributes']['description'],"<br />";
                                               echo "<br />";
                                               */

                                              $ss_con = cpi_container_storesection(
                                                $b['@attributes']['id'],
                                                $b['@attributes']['storeId'],
                                                $b['@attributes']['parentSectionId'],
                                                $b['@attributes']['caption'],
                                                $b['@attributes']['description']
                                              );

                                              $check = cpi_storesection_save($ss_con);
                                              cpi_list_storesections($b['@attributes']['id'], $store_name);

                                              $xmlurl1 = "public://cafepress/storesection" . $b['@attributes']['id'] . ".xml";
                                              if (file_exists($xmlurl1)) {
                                                $xmlstr1 = file_get_contents($xmlurl1);
                                              }
                                              else {
                                                $xmlstr1 = "";
                                              }
                                              $xmlobj1 = simplexml_load_string($xmlstr1);
                                              $arrxml1 = objectsIntoArray($xmlobj1);

                                              foreach ($arrxml1 as $c) {
                                                $cnt1 = count($c);
                                                switch ($cnt1) {
                                                  case 1:
                                                    foreach ($c as $d) {

                                                        /** for testing purpuses only
                                                         echo "second ===>",$d['id'],"<br />";
                                                         echo "second ===>",$d['storeId'],"<br />";
                                                         echo "second ===>",$d['parentSectionId'],"<br />";
                                                         echo "second ===>",$d['caption'],"<br />";
                                                         echo "second ===>",$d['description'],"<br />";
                                                         echo "<br />";
                                                         */
                                                        $ss_con = cpi_container_storesection(
                                                          $d['id'],
                                                          $d['storeId'],
                                                          $d['parentSectionId'],
                                                          $d['caption'],
                                                          $d['description']
                                                        );

                                                        $check = cpi_storesection_save($ss_con);
                                                        cpi_list_storesections($d['id'], $store_name);

                                                        $xmlurl2 = "public://cafepress/storesection" . $d['id'] . ".xml";
                                                        if (file_exists($xmlurl2)) {
                                                            $xmlstr2 = file_get_contents($xmlurl2);
                                                          }
                                                          else {
                                                              $xmlstr2 = "";
                                                            }
                                                            $xmlobj2 = simplexml_load_string($xmlstr2);
                                                            $arrxml2 = objectsIntoArray($xmlobj2);

                                                            foreach ($arrxml2 as $z) {
                                                                $cntz = count($z);
                                                                switch ($cntz) {
                                                                    case 1:
                                                                      foreach ($z as $y) {

                                                                            /** for testing purpuses only
                                                                             echo "third ===>",$y['id'],"<br />";
                                                                             echo "third ===>",$y['storeId'],"<br />";
                                                                             echo "third ===>",$y['parentSectionId'],"<br />";
                                                                             echo "third ===>",$y['caption'],"<br />";
                                                                             echo "third ===>",$y['description'],"<br />";
                                                                             echo "<br />";
                                                                             */
                                                                            $ss_con = cpi_container_storesection(
                                                                              $y['id'],
                                                                              $y['storeId'],
                                                                              $y['parentSectionId'],
                                                                              $y['caption'],
                                                                              $y['description']
                                                                            );

                                                                            $check = cpi_storesection_save($ss_con);
                                                                          }
                                                                          // end foreach ($z as $y)
                                                                          break;

                                                                        default:
                                                                          foreach ($z as $y) {

                                                                                /** for testing purpuses only
                                                                                 echo "third ===>",$y['@attributes']['id'],"<br />";
                                                                                 echo "third ===>",$y['@attributes']['storeId'],"<br />";
                                                                                 echo "third ===>",$y['@attributes']['parentSectionId'],"<br />";
                                                                                 echo "third ===>",$y['@attributes']['caption'],"<br />";
                                                                                 echo "third ===>",$y['@attributes']['description'],"<br />";
                                                                                 echo "<br />";
                                                                                 */
                                                                                $ss_con = cpi_container_storesection(
                                                                                  $y['@attributes']['id'],
                                                                                  $y['@attributes']['storeId'],
                                                                                  $y['@attributes']['parentSectionId'],
                                                                                  $y['@attributes']['caption'],
                                                                                  $y['@attributes']['description']
                                                                                );

                                                                                $check = cpi_storesection_save($ss_con);
                                                                              }
                                                                              // end foreach ($z as $y)
                                                                          }
                                                                          //end switch
                                                                        }
                                                                        // end foreach ($arrxml2 as $z)
                                                                      }
                                                                      // end foreach ($c as $d)
                                                                      break;

                                                                    default:
                                                                      foreach ($c as $d) {

                                                                          /** for testing purpuses only
                                                                           echo "second ===>",$d['@attributes']['id'],"<br />";
                                                                           echo "second ===>",$d['@attributes']['storeId'],"<br />";
                                                                           echo "second ===>",$d['@attributes']['parentSectionId'],"<br />";
                                                                           echo "second ===>",$d['@attributes']['caption'],"<br />";
                                                                           echo "second ===>",$d['@attributes']['description'],"<br />";
                                                                           echo "<br />";
                                                                           */
                                                                          $ss_con = cpi_container_storesection(
                                                                            $d['@attributes']['id'],
                                                                            $d['@attributes']['storeId'],
                                                                            $d['@attributes']['parentSectionId'],
                                                                            $d['@attributes']['caption'],
                                                                            $d['@attributes']['description']
                                                                          );

                                                                          $check = cpi_storesection_save($ss_con);
                                                                          cpi_list_storesections($d['@attributes']['id'], $store_name);

                                                                          $xmlurl2 = "public://cafepress/storesection" . $d['@attributes']['id'] . ".xml";
                                                                          if (file_exists($xmlurl2)) {
                                                                              $xmlstr2 = file_get_contents($xmlurl2);
                                                                            }
                                                                            else {
                                                                                $xmlstr2 = "";
                                                                              }
                                                                              $xmlobj2 = simplexml_load_string($xmlstr2);
                                                                              $arrxml2 = objectsIntoArray($xmlobj2);

                                                                              foreach ($arrxml2 as $z) {
                                                                                  $cntz = count($z);
                                                                                  switch ($cntz) {
                                                                                      case 1:
                                                                                        foreach ($z as $y) {

                                                                                              /** for testing purpuses only
                                                                                               echo "third ===>",$y['id'],"<br />";
                                                                                               echo "third ===>",$y['storeId'],"<br />";
                                                                                               echo "third ===>",$y['parentSectionId'],"<br />";
                                                                                               echo "third ===>",$y['caption'],"<br />";
                                                                                               echo "third ===>",$y['description'],"<br />";
                                                                                               echo "<br />";
                                                                                               */
                                                                                              $ss_con = cpi_container_storesection(
                                                                                                $y['id'],
                                                                                                $y['storeId'],
                                                                                                $y['parentSectionId'],
                                                                                                $y['caption'],
                                                                                                $y['description']
                                                                                              );

                                                                                              $check = cpi_storesection_save($ss_con);
                                                                                            }
                                                                                            // end foreach ($z as $y)
                                                                                            break;

                                                                                          default:
                                                                                            foreach ($z as $y) {

                                                                                                  /** for testing purpuses only
                                                                                                   echo "third ===>",$y['@attributes']['id'],"<br />";
                                                                                                   echo "third ===>",$y['@attributes']['storeId'],"<br />";
                                                                                                   echo "third ===>",$y['@attributes']['parentSectionId'],"<br />";
                                                                                                   echo "third ===>",$y['@attributes']['caption'],"<br />";
                                                                                                   echo "third ===>",$y['@attributes']['description'],"<br />";
                                                                                                   echo "<br />";
                                                                                                   */
                                                                                                  $ss_con = cpi_container_storesection(
                                                                                                    $y['@attributes']['id'],
                                                                                                    $y['@attributes']['storeId'],
                                                                                                    $y['@attributes']['parentSectionId'],
                                                                                                    $y['@attributes']['caption'],
                                                                                                    $y['@attributes']['description']
                                                                                                  );

                                                                                                  $check = cpi_storesection_save($ss_con);
                                                                                                }
                                                                                                // end foreach ($z as $y)
                                                                                            }
                                                                                            //end switch
                                                                                          }
                                                                                          // end foreach ($arrxml2 as $z)
                                                                                        }
                                                                                        // end foreach ($c as $d)
                                                                                    }
                                                                                    //end switch
                                                                                  }
                                                                                  // end foreach ($arrxml1 as $c)
                                                                                }
                                                                                // end foreach ($a as $b)
                                                                            }
                                                                            //end switch
                                                                          }
                                                                          // end foreach ($arrxml0 as $a)
                                                                        }
                                                                        // end function

